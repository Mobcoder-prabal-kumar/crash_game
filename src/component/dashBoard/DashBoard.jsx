import React, { useState } from 'react'
import './dashboard.css'
import { ToastContainer, toast } from "react-toastify"
import "react-toastify/dist/ReactToastify.css";
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Filler,
    Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Filler,
    Legend
);
export default function DashBoard() {
    const [chartData, setChartData] = useState({ labels: [], datasets: [{ data: [], borderColor: 'rgb(255, 99, 132)', borderWidth: 10, }] });

    const options = {
        responsive: true,
        bezierCurve: true,

        scales: {
            x: {
                ticks: {
                    color: 'white',
                },
            },
            y: {
                ticks: {
                    color: 'white',
                },
            },
        },


        plugins: {
            legend: false,
            title: {
                display: true,
                text: 'Crash Game',
                color: "white",
            },
        },
    };


    let currentMultiplier = 1;
    let currentBet = 0;
    function placeBet(betAmount) {
        if (currentBet === 0) {
            currentBet = betAmount;
            currentMultiplier = 1;
            toast.info(`New round started with a bet of ${betAmount}.`)
            increaseMultiplier();
        } else {
            alert("You already have an active bet. Wait for the round to finish.");
        }
    }
    function increaseMultiplier() {
        const intervalId = setInterval(() => {
            currentMultiplier += 0.1;
            updateChartData(currentMultiplier + 0.1)
            toast.info(`current Profit ${currentMultiplier.toFixed(2)}x`)
            if (Math.random() > 0.9) {
                clearInterval(intervalId);
                endRound();
            }
        }, 1000);
    }

    const updateChartData = (newMultiplier) => {
        setChartData((prevData) => ({
            labels: [...prevData.labels, prevData.labels.length + 1],
            datasets: [{ data: [...prevData.datasets[0].data, newMultiplier.toFixed(2)] }],
        }));
    };


    function endRound() {
        toast.success(`you won ${parseInt(currentBet * currentMultiplier)}`)
        currentBet = 0;
    }
    return (
        <section>
            <ToastContainer />
            <div className='main_div'>
                <div className='right_div'>
                    <Line options={options} data={chartData} />
                </div>
                <div className='play_button' onClick={() => placeBet(100)}>
                    <button>
                        Play
                    </button>
                </div>
            </div>
        </section>
    )
}
