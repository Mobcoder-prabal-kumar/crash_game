import './App.css';
import DashBoard from './component/dashBoard/DashBoard';

function App() {
  return (
    <>
      <DashBoard />
    </>
  );
}

export default App;
